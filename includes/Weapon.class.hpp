
#ifndef WEAPON_CLASS_HPP
# define WEAPON_CLASS_HPP

# include <iostream>
# include "Game.class.hpp"
# include "Scene.class.hpp"
# include "Bullet.class.hpp"

class Weapon
{
private:
	long int	_lastShot;
	int			_fireRate;
	Scene		*_scene;

public:
	Weapon (  );
	Weapon ( Scene & scene, int fireRate );
	Weapon ( Weapon const & _ );
	virtual ~Weapon (  );

	Weapon &	operator=( Weapon const & _ );

	int			getFireRate( void ) const;
	int			getLastShot( void ) const;
	Scene *		getScene(void) const;

	void		setFireRate( int fireRate );
	void		setLastShot(long int value);

	void		shoot(int x, int y, int direction);
};

#endif /* !WEAPON_CLASS_HPP */
