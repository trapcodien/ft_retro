#ifndef PLAYER_CLASS_HPP
# define PLAYER_CLASS_HPP

# include "Scene.class.hpp"
# include "Ship.class.hpp"

class Player: public Ship
{
public:
	Player (  );
	Player( Shape & shape, Weapon & weapon, Scene * scene );
	Player ( Player const & _ );
	virtual ~Player (  );

	Player &	operator=( Player const & _ );

	bool			move(void);

	bool			collideAndDie(GameEntity * object);
};

#endif /* !PLAYER_CLASS_HPP */
