#ifndef SHAPE_CLASS_HPP
# define SHAPE_CLASS_HPP

# include <iostream>

class Shape
{
private :
	int		_originX;
	int		_originY;
	int		_width;
	int		_height;
	int		**_shape;

public :

// Constructor
	Shape(std::string const & shape, int width, int height);

// Coplien
	Shape();
	Shape(Shape const & ref);
	Shape &			operator=(Shape const & rhs);
	~Shape();

// Getters
	int				getOriginX(void) const;
	int				getOriginY(void) const;
	int				getWidth(void) const;
	int				getHeight(void) const;
	int **			getShape(void) const;
	std::string		getStringShape(void) const;

// Setters
	void			setOriginX(int x);
	void			setOriginY(int y);
	void			setShape(int width, int height, std::string const & shape);
	void			delShape(void);
};

#endif /* !SHAPE_CLASS_HPP */

