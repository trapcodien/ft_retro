#ifndef GAME_CLASS_HPP
# define GAME_CLASS_HPP

# include "Scene.class.hpp"

# define KEY_ESC 27
# define KEY_SPACE 32
# define KEY_P 112

# define DEFAULT_FPS 60

class Game
{
private:
	time_t	_startTime;
	int		_fps;
	int		_interval;
	bool	_pause;
	bool	_end;
	Scene	_scene;

public:
// Constructor
	Game ( time_t startTime , int fps );

// Coplien
	Game (  );
	Game ( Game const & ref );
	~Game (  );
	Game &			operator=( Game const & rhs );

// Game Loop
	void				start( void );

// Getters
	time_t				getStartTime( void ) const;
	int					getFps( void ) const;
	int					getInterval( void ) const;
	bool				getPause( void ) const;
	bool				getEnd( void ) const;
	Scene const &		getScene( void ) const;

// Setters
	void				end( void );
	void				pause( void );
	void				resume( void );
	void				setFps( int fps );
	void				setScene(Scene const & ref);

// Static get time in ms
	static long int		now(void);
};

#endif /* ! */

