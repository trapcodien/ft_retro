
#ifndef SHIP_CLASS_HPP
# define SHIP_CLASS_HPP

#include "Scene.class.hpp"
# include "GameEntity.class.hpp"
# include "Weapon.class.hpp"

class Ship: public GameEntity
{
protected:
	Weapon	_weapon;


public:
	Ship (  );
	Ship( int x, int y, int direction, int speed, int life, int damage, Shape & shape, Weapon const & weapon, Scene * scene );
	Ship ( Weapon const & weapon );
	Ship ( Ship const & _ );
	virtual ~Ship (  );

	Ship &	operator=( Ship const & _ );
	Ship &	operator+=( Ship const & _ );

	Weapon const &getWeapon( void ) const;

	void		changeWeapon( Weapon const & weapon );

	void		fire( void );

	virtual bool collideAndDie(GameEntity * object);

};

#endif /* !SHIP_CLASS_HPP */
