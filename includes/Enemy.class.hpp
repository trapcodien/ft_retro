#ifndef ENEMY_CLASS_HPP
# define ENEMY_CLASS_HPP

# include "Ship.class.hpp"

class Enemy : public Ship
{
private :
	Enemy();
	int		_nbUpdate;

public :
// Coplien
	Enemy( int x, int y, int speed, int life, int damage, Shape & shape, Weapon const & weapon, Scene * scene );
	~Enemy();
	Enemy(Enemy const & ref);
	Enemy &		operator=(Enemy const & rhs);

	virtual void	update(void);
	virtual bool	collideAndDie(GameEntity * object);
};

#endif /* !ENEMY_CLASS_HPP */
