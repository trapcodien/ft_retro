
#ifndef LIST_HPP
# define LIST_HPP

class List
{
protected :
	List		*_next;
	List		*_prev;


public : 
// Coplien
	List();											// Coplien
	List(List const & ref);							// Coplien
	List &		operator=(List const & rhs);		// Coplien
	virtual ~List();								// Coplien

// Getters
	List * 		getNext(void) const;
	List * 		getPrev(void) const;

// Setters
	void		setNext(List * elem);
	void		setPrev(List * elem);

// Functions
	void		add(List & elem);
	void		addTo(List & list);
	void		del(void);
};

#endif /* !LIST_HPP */
