
#ifndef SCENE_CLASS_HPP
# define SCENE_CLASS_HPP

# define DEFAULT_WIDTH 142
# define DEFAULT_HEIGHT 42

# include <curses.h>
# include <cstdlib>
# include <ctime>
# include "GameEntity.class.hpp"
# include "List.class.hpp"

class GameEntity;
class Shape;

class Scene
{
private:
	int				_score;
	int				_lives;
	int const		_width;
	int	const		_height;
	WINDOW *		_win;
	GameEntity *	_gameEntities;
	GameEntity *	_collider[DEFAULT_WIDTH][DEFAULT_HEIGHT];
	bool			_keyUP;
	bool			_keyDOWN;
	bool			_keyFIRE;

/*
// Count-Island Algorithm
	void			countIsland(int x, int y, GameEntity * entity);
	*/

// Reset Collider
	void			resetCollider( void );

// Random Entity Generation
	int				rand(int min, int max);
	void			generateEntity(Shape & shape);

public:
// Constructor
	Scene ( WINDOW * win, GameEntity * new_player );

// Coplien
	Scene();
	Scene ( Scene const & ref );
	Scene &	operator=( Scene const & rhs );
	~Scene (  );

// General Getters
	int			getScore( void ) const;
	int			getLives( void ) const;
	int 		getWidth( void ) const;
	int			getHeight( void ) const;
	WINDOW *	getWin(void) const;
	bool		getUP( void ) const;
	bool		getDOWN( void ) const;
	bool		getFIRE( void ) const;
	GameEntity* getGameEntities(void) const;

// General Setters
	void		incrementScore( int value );
	void		decrementLives( void );
	void		setUP( bool value );
	void		setDOWN( bool value );
	void		setFIRE( bool value );
	void		setWin(WINDOW *w);

// Pointers Map ( for collide )
	GameEntity* getCollide( int x, int y ) const;
	void		setCollide( int x, int y, GameEntity * ptr );

// List Entity Management
	void		addEntity( GameEntity * new_entity );
	void		delEntity( GameEntity * entity );
	
// Game Render
	void		draw(int x,int y, char c) const;
	void		updateScene( void );
	void		renderScene( void ) const;
	void		renderStatus( void ) const;
	void		renderPause( void ) const;

};

#endif /* !SCENE_CLASS_HPP */
