#ifndef BULLET_CLASS_HPP
# define BULLET_CLASS_HPP

# include "Scene.class.hpp"
# include "Shape.class.hpp"

class Ship;

class Bullet: public GameEntity
{
private :
	Bullet (  );

public :
	Bullet( int x, int y, int direction, int speed, int damage, Shape & shape, Scene * scene );
	Bullet ( Bullet const & _ );
	virtual ~Bullet (  );

	Bullet &	operator=( Bullet const & _ );

	void		addBullet( int x, int y, int direction );
	bool		collideAndDie(GameEntity * object);
};

#endif /* !BULLET_CLASS_HPP */
