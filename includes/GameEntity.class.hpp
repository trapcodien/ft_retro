#ifndef GAME_ENTITY_HPP
# define GAME_ENTITY_HPP

# include "List.class.hpp"
# include "Scene.class.hpp"
# include "Shape.class.hpp"

# define DIR_NONE 0
# define DIR_UP 1
# define DIR_UPLEFT 2
# define DIR_LEFT 3
# define DIR_DOWNLEFT 4 
# define DIR_DOWN 5
# define DIR_DOWNRIGHT 6
# define DIR_RIGHT 7
# define DIR_UPRIGHT 8

class Scene;

class GameEntity: public List
{
protected:
	enum type { ENTITY = 0, SHIP, PLAYER, BULLET, ENEMY };

	int		_x;
	int		_y;
	int		_direction;
	int		_nbUpdate;
	int		_speed;
	int		_life;
	int		_damage;
	Shape	_shape;
	Scene *	_scene;
	type	_type;

public:
// Constructor
	GameEntity ( int x, int y, int direction, int speed, int life, int damage, Shape & shape, Scene * scene );

// Coplien
	GameEntity (  );
	GameEntity ( GameEntity const & _ );
	virtual ~GameEntity (  );
	GameEntity &	operator=( GameEntity const & _ );

// List Getters
	GameEntity *	getNext(void) const;
	GameEntity *	getPrev(void) const;

// Getters
	int				getX( void ) const;
	int				getY( void ) const;
	int				getDirection( void ) const;
	int				getNbUpdate( void ) const;
	int				getSpeed( void ) const;
	int				getLife( void ) const;
	int				getDamage( void ) const;
	Shape *			getShape( void ) const;
	Scene *			getScene( void ) const;
	type			getType( void ) const;

// Setters
	void			setX( int x );
	void			setY( int y );
	void			setDirection( int direction );
	void			setNbUpdate( int nbUpdate );
	void			setSpeed( int speed );
	void			setLife( int life );
	void			setDamage( int damage );
	void			setShape( Shape const & shape );
	void			setScene( Scene * scene );

// Functions
	virtual bool	move(void);
	virtual void	update( void );
	void			print( void );

// Collides
	virtual bool	collideAndDie(GameEntity * object);
	void			initCollide(void) const;
	GameEntity		*testCollide(void) const;

protected :
	GameEntity		*initCollide(GameEntity * object) const;

public :

};

#endif /* !GAME_ENTITY_HPP */
