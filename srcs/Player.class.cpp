#include "Player.class.hpp"

Player::Player()
{
	this->_type = GameEntity::PLAYER;
}

Player::Player( Shape & shape, Weapon & weapon, Scene * scene ):
Ship(2, 10, 0, 10, 3, 30, shape, weapon, scene) 
{
	this->_type = GameEntity::PLAYER;
}

Player::Player( Player const & player ) : Ship(player)
{
	*this = player;
}

Player::~Player()
{}

Player &	Player::operator=( Player const & player )
{
	Ship		*that = this;
	Ship const	*elem = &player;

	*that = *elem;
	return (*this);
}

bool	Player::move(void)
{
	this->setNbUpdate(this->getNbUpdate() + 1);
	if (this->getNbUpdate() >= ( 10 - this->getSpeed()))
	{
		this->setNbUpdate(0);
		if (this->_scene->getUP() && this->getY() > 1)
		{
			this->initCollide();
			this->setY(this->getY() - 1);
			return true;
		}
		if (this->_scene->getDOWN() && this->getY() < this->_scene->getHeight() - this->_shape.getHeight() - 1)
		{
			this->initCollide();
			this->setY(this->getY() + 1);
			return true;
		}
		if (this->_scene->getFIRE())
		{
			this->_weapon.shoot(this->_x + this->_shape.getWidth() + 1, this->_y + this->_shape.getHeight()/2, DIR_RIGHT);
			return true;
		}
	}
	return false;
}

bool		Player::collideAndDie(GameEntity * object)
{
	this->_life -= object->getDamage();
	for (int i = 0 ; i < object->getDamage() ; i++)
		this->_scene->decrementLives();
	if (this->_life <= 0)
		return true;
	return false;
	(void) object;
}
