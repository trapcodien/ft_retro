#include "Ship.class.hpp"

Ship::Ship() : GameEntity()
{
	this->_type = GameEntity::SHIP;
}

Ship::Ship( int x, int y, int direction, int speed, int life, int damage, Shape & shape, Weapon const & weapon, Scene * scene )
: GameEntity(x, y, direction, speed, life, damage, shape, scene)
{
	this->_type = GameEntity::SHIP;
	this->_weapon = weapon;
}

Ship::Ship( Ship const & ship)
: GameEntity(ship)
{
	this->_weapon = ship.getWeapon();
}

Ship::~Ship()
{}

Ship &			Ship::operator=(Ship const & rhs)
{
	GameEntity			*that = this;
	GameEntity const	*elem = &rhs;

	*that = *elem;
	this->_weapon = rhs.getWeapon();
	return *this;
}

Weapon const &Ship::getWeapon( void ) const { return this->_weapon; }

void		Ship::changeWeapon( Weapon const & weapon) { this->_weapon = weapon; }

void		Ship::fire( void )
{
	if (this->getDirection() == DIR_LEFT)
		(const_cast<Weapon &>(this->getWeapon())).shoot(this->getX(), this->getY() + (this->_shape.getHeight()/2), this->getDirection());
	else
		(const_cast<Weapon &>(this->getWeapon())).shoot(this->getX() + this->_shape.getWidth() + 1, this->getY() + (this->_shape.getHeight()/2), this->getDirection());
}

bool		Ship::collideAndDie(GameEntity * object)
{
	this->_life -= object->getDamage();
	if (this->_life <= 0)
		return true;
	return false;
}
