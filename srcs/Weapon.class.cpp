#include "Weapon.class.hpp"

Weapon::Weapon()
{
	}

Weapon::Weapon( Scene & scene, int fireRate ):
_lastShot(0), _fireRate(fireRate), _scene(&scene)
{
}

Weapon::~Weapon()
{}

Weapon &	Weapon::operator=( Weapon const & weapon )
{
	this->_lastShot = 0;
	this->_fireRate = weapon.getFireRate();
	this->_scene = weapon.getScene();
	return (*this);
}

int			Weapon::getFireRate( void ) const { return this->_fireRate; }
int			Weapon::getLastShot( void ) const { return this->_lastShot; }
Scene *		Weapon::getScene(void) const { return this->_scene; }

void		Weapon::setFireRate( int fireRate ) { this->_fireRate = fireRate; }
void		Weapon::setLastShot(long int value) { this->_lastShot = value; }

void		Weapon::shoot(int x, int y, int direction )
{
	long int	now = Game::now();
	GameEntity	*projectile;
	Shape      shape("~", 1, 1);

	if (this->_fireRate != 0 && now - this->_lastShot > this->_fireRate)
	{
		projectile = new Bullet(x, y, direction, 10, 1, shape, this->_scene);
		this->_scene->addEntity(projectile);
		this->setLastShot(now);
	}
}
