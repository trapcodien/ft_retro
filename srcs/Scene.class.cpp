
#include <iostream>
#include <curses.h>
#include "GameEntity.class.hpp"
#include "Ship.class.hpp"
#include "Weapon.class.hpp"
#include "Scene.class.hpp"
#include "Player.class.hpp"
#include "Enemy.class.hpp"

/*
	// Private Count-Island Algorithm
void		Scene::countIsland(int x, int y, GameEntity * entity)
{
	if (!entity || x < 0 || y < 0 || x >= this->_width || y >= this->_height)
		return ;
	if (this->_collider[x][y] == entity)
	{
		this->countIsland(++x, y, entity);
		this->countIsland(x, ++y, entity);
		this->countIsland(--x, y, entity);
		this->countIsland(x, --y, entity);

		this->countIsland(++x, ++y, entity);
		this->countIsland(++x, --y, entity);
		this->countIsland(--x, --y, entity);
		this->countIsland(--x, ++y, entity);
		
		this->_collider[x][y] = NULL;
	}
}
*/

	// Constructor
Scene::Scene(WINDOW *win, GameEntity *new_player)
: _width(DEFAULT_WIDTH), _height(DEFAULT_HEIGHT), _win(win), _gameEntities(new GameEntity)
{
	this->_score = 0;
	this->_lives = 3;
	this->resetCollider();
	this->addEntity(new_player);
	this->setCollide(new_player->getX(), new_player->getY(), new_player);
	this->_keyUP = false;
	this->_keyDOWN = false;
	this->_keyFIRE = false;
}

	// Coplien
Scene::Scene() : _width(DEFAULT_WIDTH), _height(DEFAULT_HEIGHT), _gameEntities(new GameEntity)
{
	Shape		shape0("/-\\+->\\-/", 3, 3);
	Shape		shape1;

	Shape		wall(	"[" \
						"[" \
						"[" \
						"[" \
						"[", 1, 5);


	Weapon		weapon(*this, 150);
	Weapon		nothing(*this, 0);

	this->_score = 0;
	this->_lives = 3;
//	this->addEntity(new Ship(DEFAULT_WIDTH - 5, 10, DIR_LEFT, 7, 1, 1, shape1, weapon, this));
//
//
	this->addEntity(new Player(shape0, weapon, this));
	
	this->resetCollider();
	this->_win = NULL;
	this->_keyUP = false;
	this->_keyDOWN = false;
	this->_keyFIRE = false;
}

void		Scene::renderStatus( void ) const {
	box(this->getWin(), 0, 0);
	mvwprintw( this->getWin(), 0, 2," Lives %d -- Score : %d ", this->getLives(), this->getScore() );
}

Scene::Scene(Scene const & ref) : _width(ref.getWidth()), _height(ref.getHeight())
{
	*this = ref;
}

Scene & 		Scene::operator=( Scene const & rhs )
{
	GameEntity	*cursor;

	this->_score = rhs.getScore();
	this->_lives = rhs.getLives();
	this->_win = rhs.getWin();
	this->_keyUP = rhs.getUP();
	this->_keyDOWN = rhs.getDOWN();
	this->_keyFIRE = rhs.getFIRE();

	// Set _gameEntities
	cursor = rhs.getGameEntities();
	while (cursor)
	{
		this->addEntity(new GameEntity(*cursor));
		cursor = cursor->getNext();
	}

	// Set _collider[][]
	for (int x = 0 ; x < DEFAULT_WIDTH ; x++)
	{
		for (int y = 0 ; y < DEFAULT_HEIGHT ; y++)
			this->_collider[x][y] = rhs.getCollide(x, y);
	}
	return *this;
}

Scene::~Scene()
{
	GameEntity	*ptr;

	delwin(this->_win);
	while (this->_gameEntities)
	{
		ptr = this->_gameEntities;
		this->_gameEntities = this->_gameEntities->getNext();
		this->delEntity(ptr);
	}
}

	// General Getters
int				Scene::getScore( void ) const { return this->_score; }
int				Scene::getLives( void ) const { return this->_lives; }
int 			Scene::getWidth(void) const { return this->_width; }
int 			Scene::getHeight(void) const { return this->_height; }
WINDOW * 		Scene::getWin(void) const { return this->_win; }
bool			Scene::getUP(void) const { return this->_keyUP; }
bool			Scene::getDOWN(void) const { return this->_keyDOWN; }
bool			Scene::getFIRE(void) const { return this->_keyFIRE; }
GameEntity * 	Scene::getGameEntities(void) const { return this->_gameEntities; }

	// General Setters
void			Scene::incrementScore( int value ) { this->_score = this->_score + value; }
void			Scene::decrementLives( void ) { this->_lives = this->_lives - 1; }
void			Scene::setUP(bool value) { this->_keyUP = value; }
void			Scene::setDOWN(bool value) { this->_keyDOWN = value; }
void			Scene::setFIRE(bool value) { this->_keyFIRE = value; }
void			Scene::setWin(WINDOW *w) { this->_win = w; }

	// Pointers Map ( for collide )
GameEntity * 	Scene::getCollide(int x, int y) const
{
	if (x < 0 || y < 0 || x >= DEFAULT_WIDTH || y >= DEFAULT_HEIGHT)
		return (NULL);
	return (this->_collider[x][y]);
}

void			Scene::setCollide(int x, int y, GameEntity * ptr)
{
	if (x < 0 || y < 0 || x >= DEFAULT_WIDTH || y >= DEFAULT_HEIGHT)
		return ;
	this->_collider[x][y] = ptr;
}

void			Scene::resetCollider(void)
{
	for (int x = 0 ; x < this->_width ; x++)
	{
		for (int y = 0 ; y < this->_height ; y ++)
			this->_collider[x][y] = NULL;
	}
}

void			Scene::generateEntity(Shape & shape)
{
	int				rand;
	static int update = 0;
	static int until = 0;

	Weapon		weapon(*this, 150);
	Weapon		nothing(*this, 0);
	Shape		asteroid(	"/----\\" \
							"|    |" \
							"\\----/", 6, 3);
	Shape		wall(	"[" \
						"[" \
						"[" \
						"[" \
						"[", 1, 5);

	update++;
	if (update >= until)
	{
		update = 0;
		until = this->rand(60, 250);
		if ((rand = this->rand(0, 3) < 1))
		{
			this->addEntity(new Enemy(DEFAULT_WIDTH, this->rand(0, DEFAULT_HEIGHT - 5), this->rand(2, 7), 20, 3, asteroid, nothing, this));
			if (this->rand(0, 1) == 1)
				this->addEntity(new Enemy(DEFAULT_WIDTH, this->rand(0, DEFAULT_HEIGHT - 5), this->rand(2, 7), 10, 1, wall, nothing, this));
		}
		else
			this->addEntity(new Enemy(DEFAULT_WIDTH, this->rand(0, DEFAULT_HEIGHT - 5), this->rand(2, 7), 3, 1, shape, weapon, this));
	}
}

int				Scene::rand(int min, int max)
{
	static bool firstTime = true;

	if (firstTime)
	{
		firstTime = false;
		std::srand(std::time(0));	
	}
	return ((std::rand() % (max - min + 1)) + min);
}

	// List Entity Management
void			Scene::addEntity(GameEntity * new_entity)
{
	if (new_entity)
		this->_gameEntities->add(*new_entity);
}

void			Scene::delEntity(GameEntity * entity)
{
	if (entity)
	{
		/*
		// Count Island Algorithm
		this->countIsland(entity->getX(), entity->getY(), entity);
		*/

		if (entity->getScene())
			entity->initCollide();

		// Destroy entity
		if (this->_gameEntities == entity)
			this->_gameEntities = entity->getNext();
		delete entity;
	}
}


void			Scene::draw(int x, int y, char c) const
{
	mvwaddch(this->_win, y, x, c);
}

void			Scene::updateScene(void)
{
	GameEntity	*cursor;
	GameEntity	*entity;
	GameEntity	*collided;
	bool		delete_entity = false;
	bool		delete_collided = false;
	Shape		shape;

	this->generateEntity(shape);

	cursor = this->_gameEntities->getNext();
	while (cursor)
	{
		entity = cursor;
		cursor = cursor->getNext();
		delete_entity = false;
		delete_collided = false;
		
		//Delete entity if is out of screen.
		if (	(entity->move()) && (entity->getX() >= this->_width
				|| entity->getY() >= this->_height
				|| entity->getX() < -entity->getShape()->getWidth()
				|| entity->getY() <= -entity->getShape()->getHeight()))
			this->delEntity(entity);
		else {
			entity->update();
			collided = entity->testCollide();
			if (collided && entity->collideAndDie(collided))
 				delete_entity = true;
			if (collided && collided->collideAndDie(entity))
				delete_collided = true;

			if (delete_collided && collided != entity)
				this->delEntity(collided);
			if (delete_entity)
				this->delEntity(entity);

			if (delete_entity || delete_collided)
				cursor = this->_gameEntities->getNext();
		}
	}
}

void 			Scene::renderPause() const {
	mvwprintw(this->getWin(), DEFAULT_HEIGHT / 2 - 2, DEFAULT_WIDTH / 2 - 3, "PAUSE");
}

void			Scene::renderScene(void) const
{
	GameEntity	*cursor;

	cursor = this->_gameEntities->getNext();
	wclear(this->_win);
	while (cursor)
	{
		cursor->print();
		cursor = cursor->getNext();
	}
}
