
#include <iostream>
#include "GameEntity.class.hpp"

GameEntity::GameEntity ( int x, int y, int direction, int speed, int life, int damage, Shape & shape, Scene * scene ) : 
_x(x), _y(y), _direction(direction), _nbUpdate(0), _speed(speed), _life(life), _damage(damage), _shape(shape), _scene(scene)
{
	this->_type = GameEntity::ENTITY;
}

GameEntity::GameEntity()
: List(), _x(0), _y(0), _direction(0), _nbUpdate(0), _speed(10), _life(1), _damage(1), _shape(Shape()), _scene(NULL)
{
}

GameEntity::GameEntity( GameEntity const & _ ) : List()
{
	*this = _;
}

GameEntity::~GameEntity()
{
}

GameEntity &	GameEntity::operator=( GameEntity const & entity )
{
	List		*that = this;
	List const	*elem = &entity;

	*that = *elem;
	this->_x = entity.getX();
	this->_y = entity.getY();
	this->_direction = entity.getDirection();
	this->_speed = entity.getSpeed();
	this->_nbUpdate = entity.getNbUpdate();
	this->_life = entity.getLife();
	this->_damage = entity.getDamage();
	this->_shape = *(entity.getShape());
	this->_scene = entity.getScene();
	this->_type = entity.getType();
	return *this;
}

GameEntity *	GameEntity::getNext(void) const { return (GameEntity *)List::getNext(); }
GameEntity *	GameEntity::getPrev(void) const { return (GameEntity *)List::getPrev(); }

int				GameEntity::getX( void ) const { return this->_x; }
int				GameEntity::getY( void ) const { return this->_y; }
int				GameEntity::getDirection( void ) const { return this->_direction; }
int				GameEntity::getSpeed( void ) const { return this->_nbUpdate; }
int				GameEntity::getNbUpdate( void ) const { return this->_speed; }
int				GameEntity::getLife( void ) const { return this->_life; }
int				GameEntity::getDamage( void ) const { return this->_damage; }
Shape *			GameEntity::getShape( void ) const { return (Shape *) &(this->_shape); }
Scene *			GameEntity::getScene( void ) const { return this->_scene; }

GameEntity::type	GameEntity::getType( void ) const { return this->_type; }

void			GameEntity::setX( int x ) { this->_x = x; }
void			GameEntity::setY( int y ) { this->_y = y; }
void			GameEntity::setDirection( int dir ) { this->_direction = dir; }
void			GameEntity::setNbUpdate( int nb ) { this->_nbUpdate = nb; }
void			GameEntity::setSpeed( int speed ) { this->_speed = speed; }
void			GameEntity::setLife( int life ) { this->_life = life; }
void			GameEntity::setDamage( int damage ) { this->_damage = damage; }
void			GameEntity::setShape( Shape const & shape ) { this->_shape = shape; }
void			GameEntity::setScene( Scene * scene ) { this->_scene = scene; }

bool			GameEntity::move(void)
{
	this->_nbUpdate++;
	this->initCollide();
	if (this->_nbUpdate >= (10 - this->_speed))
	{
		this->_nbUpdate = 0;
		if (this->_direction == DIR_UP)
		{
			this->_y--;
		}
		if (this->_direction == DIR_UPLEFT)
		{
			this->_x--;
			this->_y--;
		}
		if (this->_direction == DIR_LEFT)
		{
			this->_x--;
		}
		if (this->_direction == DIR_DOWNLEFT)
		{
			this->_x--;
			this->_y++;
		}
		if (this->_direction == DIR_DOWN)
		{
			this->_y++;
		}
		if (this->_direction == DIR_DOWNRIGHT)
		{
			this->_x++; 
			this->_y++;
		}
		if (this->_direction == DIR_RIGHT)
		{
			this->_x++;
		}
		if (this->_direction == DIR_UPRIGHT)
		{
			this->_x++;
			this->_y--;
		}
		return true;
	}
	return false;
}

void			GameEntity::update( void )
{
}

void			GameEntity::print( void )
{
	int			width = this->_shape.getWidth();
	int			height = this->_shape.getHeight();
	int			startX = this->_x - this->_shape.getOriginX();
	int			startY = this->_y - this->_shape.getOriginY();
	int			**shape = this->_shape.getShape();

	for (int y = 0 ; y < height ; y++)
	{
		startX = this->_x - this->_shape.getOriginX();
		for (int x = 0 ; x < width ; x++)
		{
			if (shape[y][x] != ' ')
				this->_scene->draw(startX, startY, (char)shape[y][x]);
			startX++;
		}
		startY++;
	}
}

bool			GameEntity::collideAndDie(GameEntity * object)
{
	return true;
	(void) object;
}

void			GameEntity::initCollide(void) const
{
	this->initCollide(0);
}

GameEntity *	GameEntity::testCollide(void) const
{
	return this->initCollide(const_cast<GameEntity *>(this));
}

// Protected
GameEntity *	GameEntity::initCollide(GameEntity * object) const
{
	GameEntity	*collidedObject;
	int h;
	int w;

	for (h = 0 ; h < this->_shape.getHeight() ; h++)
	{
		for (w = 0 ; w < this->_shape.getWidth() ; w++)
		{
			if (this->_shape.getShape()[h][w] != ' ')
			{
				if (object && (collidedObject = this->_scene->getCollide(this->_x + w, this->_y + h)))
				{
					if (object != collidedObject)
						return collidedObject;
				}
				else
					this->_scene->setCollide(this->_x + w, this->_y + h, object);
			}
		}
	}
	return 0;
}
