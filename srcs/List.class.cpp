/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   List.class.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/10 23:54:39 by garm              #+#    #+#             */
/*   Updated: 2015/01/11 07:07:05 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "List.class.hpp"

	// Coplien
List::List() : _next(NULL), _prev(NULL) {  }
List::~List() { this->del(); }
List::List(List const & ref) { *this = ref; }
List &			List::operator=(List const & rhs)
{
	this->_next = rhs.getNext();
	this->_prev = rhs.getPrev();
	return (*this);
}

	// Getters
List *			List::getNext(void) const { return (this->_next); }
List *			List::getPrev(void) const { return (this->_prev); }

	// Setters
void			List::setNext(List * elem) { this->_next = elem; }
void			List::setPrev(List * elem) { this->_prev = elem; }
	

	// Functions
void			List::addTo(List & list) { list.add(*this); }

void			List::add(List & elem)
{
	elem.setPrev(this);
	elem.setNext(this->_next);
	if (this->_next)
		this->_next->setPrev(&elem);
	this->setNext(&elem);
}

void			List::del(void)
{
	if (this->_next)
		this->_next->setPrev(this->_prev);
	if (this->_prev)
		this->_prev->setNext(this->_next);
	this->_next = NULL;
	this->_prev = NULL;
}
