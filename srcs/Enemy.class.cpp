#include "Enemy.class.hpp"

	// Coplien
Enemy::Enemy() : Ship()
{
	this->_type = GameEntity::ENEMY;
	this->_nbUpdate = 0;
}

Enemy::Enemy(int x, int y, int speed, int life, int damage, Shape & shape, Weapon const & weapon, Scene * scene) : 
Ship(x, y, DIR_LEFT, speed, life, damage, shape, weapon, scene)
{
	this->_type = GameEntity::ENEMY;
	this->_nbUpdate = 0;
}

Enemy::~Enemy()
{
	
}

Enemy::Enemy(Enemy const & ref) : Ship(ref)
{
	*this = ref;
}

Enemy &	Enemy::operator=(Enemy const & rhs)
{
	Ship::operator=(rhs);
	return (*this);
}

bool	Enemy::collideAndDie(GameEntity * object)
{
	if (Ship::collideAndDie(object))
	{
		this->_scene->incrementScore(5);
		return true;
	}
	return false;
}

void	Enemy::update(void)
{
	this->_nbUpdate++;
	if (this->_nbUpdate == 50)
	{
		this->_nbUpdate = 0;
		this->fire();
	}
}
