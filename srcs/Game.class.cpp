
#include "sys/time.h"
#include "unistd.h"
#include "curses.h"
#include <ctime>
#include "Game.class.hpp"

	// Constructor
Game::Game(time_t startTime, int fps)
: _startTime(startTime), _fps(fps), _interval(1000/fps), _pause(false), _end(false)
{
	initscr();
	this->_scene.setWin(newwin(DEFAULT_HEIGHT, DEFAULT_WIDTH, 0, 0));
	box(this->_scene.getWin(), 0, 0);
	wrefresh(this->_scene.getWin());

	noecho();
	curs_set(FALSE);
	wtimeout(stdscr, 0);
	keypad(stdscr, TRUE);
	refresh();
}

	// Coplien
Game::Game()
: _startTime(time(NULL)), _fps(DEFAULT_FPS), _interval(1000/DEFAULT_FPS), _pause(false), _end(false)
{
	initscr();
	this->_scene.setWin(newwin(DEFAULT_HEIGHT, DEFAULT_WIDTH, 0, 0));
	box(this->_scene.getWin(), 0, 0);
	wrefresh(this->_scene.getWin());

	noecho();
	curs_set(FALSE);
	wtimeout(stdscr, 0);
	keypad(stdscr, TRUE);
	refresh();
}

Game::Game(Game const & ref)
{
	*this = ref;
}

Game::~Game() { endwin(); std::cout << "End of Game." << std::endl; }

Game &			Game::operator=(Game const & rhs)
{
	this->_startTime = rhs.getStartTime();
	this->_fps = rhs.getFps();
	this->_pause = rhs.getPause();
	this->_end = rhs.getEnd();
	this->_scene = rhs.getScene();
	return *this;
}

	// Game Loop
void			Game::start(void)
{
	long int	waitingTime = 0;
	long int	ms = Game::now();
	int			key;
	char		*keyPtr = (char *)&key;
	(void)keyPtr;

	while (!this->_end)
	{

		if (this->_scene.getLives() <= 0)
		{
			wclear(this->_scene.getWin());
			mvwprintw(this->_scene.getWin(), DEFAULT_HEIGHT / 2 - 2, DEFAULT_WIDTH / 2 - 3, "YOU LOOSE : %d pts", this->_scene.getScore());
			wrefresh(this->_scene.getWin());
			while (getch() != KEY_ESC)
				;
			end();
		}
		
		waitingTime = (this->_interval - (Game::now() - ms));
		if (waitingTime < 0)
			waitingTime = 0;
		usleep(waitingTime * 1000);

		// The Mighty input if Forest
		key = getch();
		if (key == KEY_ESC)
			this->end();
		if (key == KEY_P && this->_pause)
			this->resume();
		else if (key == KEY_P && !this->_pause)
			this->pause();

		if (key == KEY_SPACE)
			this->_scene.setFIRE(true);
		else
			this->_scene.setFIRE(false);

		if (key == KEY_UP)
			this->_scene.setUP(true);
		else
			this->_scene.setUP(false);

		if (key == KEY_DOWN)
			this->_scene.setDOWN(true);
		else
			this->_scene.setDOWN(false);

		if (!this->getPause())
		{
			this->_scene.updateScene();
			this->_scene.renderScene();
			this->_scene.renderStatus();
		} else {
			this->_scene.renderPause();
		}
		wrefresh(this->_scene.getWin());
		ms = Game::now();
	}
}

	// Getters
time_t			Game::getStartTime(void) const { return this->_startTime; }
int				Game::getFps(void) const { return this->_fps; }
int				Game::getInterval(void) const { return this->_interval; }
bool			Game::getPause(void) const { return this->_pause; }
bool			Game::getEnd(void) const { return this->_end; }
Scene const & 	Game::getScene(void) const { return this->_scene; }

	// Setters
void			Game::end(void) { this->_end = true; }
void			Game::pause(void) { this->_pause = true; }
void			Game::resume(void) { this->_pause = false ; }

void			Game::setFps(int fps)
{
	if (!fps)
		fps = DEFAULT_FPS;
	fps *= (fps < 0) ? (-1) : (1);
	this->_fps = fps;
	this->_interval = 1000 / fps;
}

void			Game::setScene(Scene const & ref) { this->_scene = ref; }

long int		Game::now(void)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);
	return ((tv.tv_sec * 1000) + (tv.tv_usec / 1000));
}

