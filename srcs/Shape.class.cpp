
#include "Shape.class.hpp"

	// Constructor
Shape::Shape(std::string const & shape, int width, int height)
: _originX(0), _originY(0), _width(width), _height(height), _shape(NULL) 
{
	this->setShape(width, height, shape);
}

	//Coplien
Shape::Shape() : _originX(0), _originY(0), _width(15), _height(6), _shape(NULL)
{
	/*
	this->setShape(this->_width, this->_height, "8=o)- 8=o"); // 3 * 3
	*/
//	this->setShape(this->_width, this->_height, "o===8(-        (-   o===8");
	std::string		new_shape( \
	"      _~_      " \
	"   __(__(__    " \
	"  (_((_((_(    " \
	"\\=-:--:--:--.=~" \
	" \\_o__o__o_/ =~" \
	"               ");
	this->setShape(this->_width, this->_height, new_shape);
}

Shape::Shape(Shape const & ref) : _shape(NULL)
{
	*this = ref;
}

Shape & 			Shape::operator=(Shape const & rhs)
{
	this->_originX = rhs.getOriginX();
	this->_originY = rhs.getOriginY();
	this->setShape(rhs.getWidth(), rhs.getHeight(), rhs.getStringShape());
	return (*this);
}

Shape::~Shape()
{
	this->delShape();
}

// Getters
int				Shape::getOriginX(void) const { return this->_originX; }
int				Shape::getOriginY(void) const { return this->_originY; }
int				Shape::getWidth(void) const { return this->_width; }
int				Shape::getHeight(void) const { return this->_height; }
int **			Shape::getShape(void) const { return this->_shape; }

std::string		Shape::getStringShape(void) const
{
	std::string	stringShape = "";

	if (this->_shape == NULL)
		return stringShape;

	for (int x = 0 ; x < this->_height ; x++)
	{
		for (int y = 0; y < this->_width ; y++)
			stringShape += this->_shape[x][y];
	}
	return stringShape;
}

// Setters
void			Shape::setOriginX(int x)
{
	if (x >= this->_width || x < 0)
		return ;
	this->_originX = x;
}

void			Shape::setOriginY(int y)
{
	if (y >= this->_height || y < 0)
		return ;
	this->_originY = y;
}

void			Shape::setShape(int width, int height, std::string const & shape)
{
	int			stringCursor = 0;

	if (((width * height) != (int) shape.length()) || width < 0 || height < 0)
		return ;
	this->delShape();
	this->_width = width;
	this->_height = height;
	this->_shape = new int*[height];
	for (int i = 0 ; i < height ; i++)
	{
		this->_shape[i] = new int[width];
		for (int j = 0 ; j < width ; j++)
		{
			this->_shape[i][j] = (int)shape.at(stringCursor);
			stringCursor++;
		}
	}

}

void			Shape::delShape(void)
{
	if (this->_shape)
	{
		for (int i = 0 ; i < this->_height ; i++)
			delete [] this->_shape[i];
		delete [] this->_shape;
	}
}
