/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/11 00:41:15 by garm              #+#    #+#             */
/*   Updated: 2015/01/11 14:43:42 by jvincent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <curses.h>
#include "Game.class.hpp"
#include "Scene.class.hpp"
#include "Shape.class.hpp"
#include "GameEntity.class.hpp"

int		main(void)
{
	Game game;
	game.start();
	return (0);
}
