#include "Bullet.class.hpp"

Bullet::Bullet() : GameEntity()
{
	this->_type = GameEntity::BULLET;
}

Bullet::Bullet( int x, int y, int direction, int speed, int damage, Shape & shape, Scene * scene )
: GameEntity( x, y, direction, speed, 1, damage, shape, scene)
{
	this->_type = GameEntity::BULLET;
}

Bullet::Bullet( Bullet const & bullet) : GameEntity(bullet)
{
	*this = bullet;
}

Bullet &	Bullet::operator=( Bullet const & entity )
{
	this->_x = entity.getX();
	this->_y = entity.getY();
	this->_direction = entity.getDirection();
	this->_speed = entity.getSpeed();
	this->_nbUpdate = entity.getNbUpdate();
	this->_life = entity.getLife();
	this->_damage = entity.getDamage();
	this->_shape = *(entity.getShape());
	this->_scene = entity.getScene();
	return *this;
}

Bullet::~Bullet()
{}

void	Bullet::addBullet( int x, int y, int direction )
{
	Bullet * b = new Bullet(*this);
	b->setX(x);
	b->setY(y);
	b->setDirection(direction);
	this->getScene()->addEntity(b);
}

bool	Bullet::collideAndDie(GameEntity * object)
{
	return true;
	(void) object;
}
