#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jvincent <jvincent@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/11/19 18:33:50 by jvincent          #+#    #+#              #
#    Updated: 2015/04/03 14:33:18 by jvincent         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

RESET = 		\x1b[0m
RED = 			\x1b[33;01m
GREEN = 		\x1b[32;01m

CC = 			g++

NAME = 			ft_retro

SRCDIR =		srcs/
INCDIR =		includes/

CFLAGS = 		-Wpadded -Wall -Werror -Wextra

ifeq ($(DEBUG), 1)
	FLAGS = -g -Wall -Wextra -std=c++98
else
	FLAGS = -Wall -Werror -Wextra -std=c++98
endif

CFLAGS += $(FLAGS) -I $(INCDIR) -lncurses

DEPENDENCIES = \
				List.class.hpp \
				Game.class.hpp \
				Scene.class.hpp \
				Shape.class.hpp \
				GameEntity.class.hpp \
				Ship.class.hpp \
				Bullet.class.hpp \
				Enemy.class.hpp \

SRCSFILES =		\
				main.cpp \
				List.class.cpp \
				Game.class.cpp \
				Scene.class.cpp \
				Shape.class.cpp \
				GameEntity.class.cpp \
				Ship.class.cpp \
				Weapon.class.cpp \
				Player.class.cpp \
				Bullet.class.cpp \
				Enemy.class.cpp \

SRCS =			$(addprefix $(SRCDIR), $(SRCSFILES))
INCS =			$(addprefix $(INCDIR), $(DEPENDENCIES))
OBJS =			$(SRCS:.cpp=.o)

all: $(NAME)

%.o: %.cpp $(INCS)
	@$(CC) $(FLAGS) -I $(INCDIR) -c $< -o $@ && echo "[$(GREEN)OK$(RESET)] $<"

$(NAME): $(OBJS)
	$(CC) -o $(NAME) $(CFLAGS) $(OBJS)
	@echo "$(GREEN)Binary successfully compiled$(RESET)" : $(NAME)

clean:
	@rm -f $(OBJS) && echo "[$(RED)DEL$(RESET)] $(NAME) objects"

fclean: clean
	@rm -f $(NAME) && echo "[$(RED)DEL$(RESET)] $(NAME)"
	@rm -rf $(NAME).dSYM && echo "[$(RED)DEL$(RESET)] $(NAME).dSYM"

test: re
	@echo "" && echo "$(GREEN)./$(NAME)$(RESET)" && ./$(NAME)

re: fclean all

.PHONY: clean fclean re all
